package log

import (
	"fmt"
	"github.com/rs/zerolog"
	"os"
	"time"
)

type logger struct {
	log zerolog.Logger
}

func NewZeroLog(appName, appVersion string, level Level) *logger {

	switch level {
	case Error:
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	case Warn:
		zerolog.SetGlobalLevel(zerolog.WarnLevel)
	case Info:
		zerolog.SetGlobalLevel(zerolog.InfoLevel)
	case Debug:
		zerolog.SetGlobalLevel(zerolog.DebugLevel)
	default:
		zerolog.SetGlobalLevel(zerolog.ErrorLevel)
	}

	zerolog.TimestampFieldName = "timestamp"
	zerolog.TimestampFunc = func () time.Time {
		return time.Now().UTC()
	}

	ctx := zerolog.New(os.Stdout).With().Timestamp()

	ctx = ctx.Dict("program", zerolog.Dict().Fields(map[string]interface{}{
		"name": appName,
		"version": appVersion,
	}))

	return &logger{ctx.Logger()}
}

func (l *logger) Fatal(message string, args ...interface{}) {
	l.log.Fatal().Msg(fmt.Sprintf(message, args...))
}

func (l *logger) Error(message string, args ...interface{}) {
	l.log.Error().Msg(fmt.Sprintf(message, args...))
}

func (l *logger) Warn(message string, args ...interface{}) {
	l.log.Warn().Msg(fmt.Sprintf(message, args...))
}

func (l *logger) Info(message string, args ...interface{}) {
	l.log.Info().Msg(fmt.Sprintf(message, args...))
}

func (l *logger) Debug(message string, args ...interface{}) {
	l.log.Debug().Msg(fmt.Sprintf(message, args...))
}