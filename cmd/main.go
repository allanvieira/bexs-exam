package main

import (
	"bitbucket.org/allanvieira/bexs-exam/domain/route"
	"bitbucket.org/allanvieira/bexs-exam/domain/station"
	"bitbucket.org/allanvieira/bexs-exam/domain/trip"
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/csv"
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/server/api"
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/server/console"
	"bitbucket.org/allanvieira/bexs-exam/internal/repository/memory"
	"bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"bufio"
	"errors"
	"os"
)

const (
	envAPPPort     = "APP_PORT"
	envLoggerLevel = "LOGGER_LEVEL"

	applicationName    = "BEXS-EXAM"
	applicationVersion = "1.0.0"
)

func main() {
	log := log.NewZeroLog(applicationName, applicationVersion, log.Level(getLoggerLevel()))

	//Check File Path in go exec parameter
	filePath, errParam := handleFileParam()
	if errParam != nil {
		log.Error("Error on check params : %s", errParam)
	}

	// CSV File
	csv, errCsv := csv.New(filePath, log)
	if errCsv != nil {
		log.Error("Error on create new csv reader : %s", errCsv)
	}

	// Repositories
	routeRepositoryMemory := memory.NewRouteMemory()
	stationRepositoryMemory := memory.NewStationMemory()

	//Services
	stationServices := station.NewService(stationRepositoryMemory)
	tripServices := trip.NewService(stationRepositoryMemory)
	routeServices := route.NewService(csv, routeRepositoryMemory, stationServices, tripServices)

	//Handler
	handler := api.NewHandler(log, tripServices, stationServices, routeServices)

	//Server
	APIServer := api.New(getApplicationPort(), handler, log)
	consoleServer := console.New(bufio.NewReader(os.Stdin), tripServices, log)


	// Import CSV Routes to repository
	_, errImport := routeServices.ImportCSV()
	if errImport != nil {
		log.Error("Error on import csv : %s", errImport)
	}

	APIServer.ListenAndServe()
	consoleServer.ListenAndServe()

}

func handleFileParam() (param string, err error) {
	if len(os.Args) < 2 {
		err = errors.New("inform path from csv that contain the routes to import (SRC,DST,COST)")
		return
	}
	param = os.Args[1]
	return
}

func getLoggerLevel() string {
	value := os.Getenv(envLoggerLevel)
	if value != "" {
		return value
	}
	return "error"
}

func getApplicationPort() string {
	value := os.Getenv(envAPPPort)
	if value != "" {
		return value
	}
	return "8080"
}
