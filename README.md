#Bexs Backend Exam : Trip Route

## Getting Started

### Environment variables

```bash
APP_PORT=8080 #optional, default 8080
LOGGER_LEVEL=error ##optional, default error, values : error; warn; info; debug;
```

### Installing and running locally
```shell script
go run cmd/main.go /path/to/file.csv
``` 

### Build
```shell script
go build cmd/main.go 
```

### Installing and running binary
```shell script
./trip_route_service /path/to/file.csv
```

## How to use

### Console Interface

Wait for instructions from the console and send the input data, example input: ORG-DST

```shell script
please enter the route: GRU-CDG
best route: GRU - BRC - SCL - ORL - CDG > $40
please enter the route:
```

### API Interface

- Insert new route :
```shell script
curl --location --request POST 'localhost:8080/v1/route' \
--header 'Content-Type: application/json' \
--data-raw '{
    "origin": "PTB",
    "destination": "FLN",
    "cost": 200
}'
```

- Search best route :
```shell script
curl --location --request GET 'localhost:8080/v1/trip/best?origin=GRU&destination=CDG'
```

## Architecture

The architecture used in this project was inspired using the name of the packages from [golang-standards](https://github.com/golang-standards/project-layout), the organization of the project layers was inspired using **Hexagonal Architeture** (Ports & Adapters).
Is based on Ports using interfaces, and Adapters using the interface implementations. 

### Packages 
|NAME | DESCRIPTION |
|--- | --- |
|cmd | Main Applications for this project, have a small `main` function that imports and invokes the code from the `/domain/$PKG_NAME`, `/internal` and `/pkg` |
|domain | This package have models that contain all the business logic of the application, looking at the files inside this package it is possible to understand what the application is doing. The files contained in this package do not use files from other packages. Mostly they are structs and interfaces that encapsulate the business rule and the connection to the `/internal` and`/domain/$PKG_NAME` packages.  |
|domain/$PKG_NAME| These packages contain the domain services, these services are located in the middle layer of the proposed architecture, read the domain models and are used for more external `/internal` layers. The services in these packages contain the business rule to be tested. |
|internal| This package contains the code that represents the outermost layer of the proposed architecture. Contains files of type handdler, event listeners, respositories, persistence, etc. Basically contains user interfaces and connections to infrastructure  |
|internal/infrastructure| This package contains ways to connect to external applications, for example: SFTP, CSV, HTTP Client, Server API, Console, Databases, Email, Git, etc.  |
|internal/repository| This package contains the data repositories responsible for implementing the `domain` package interface contracts. Each package within this package corresponds to an example persistence type: `repository/postgres`,` repository/memory`, `repository/elasticsearch`  |

### Justification

I believe that through the proposed architecture it is possible to increase the degree of understanding of the business rule through an isolated view, this brings a very big gain in reuse of code and ease of reusing the business rule between various user interfaces.

The way in which each infrastructure is handled facilitates the connection with several existing external applications, this service is ready to integrate with other external connections without having to change the business layer code.