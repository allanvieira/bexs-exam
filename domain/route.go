package domain

type Route struct {
	Origin      string
	Destination string
	Cost        float64
}

type RouteService interface {
	ImportCSV() ([]*Route, error)
	Create(route *Route) (*Route, error)
	IsValid(route *Route) error
}

type RouteRepository interface {
	Insert(route *Route) error
	Update(route *Route) error
	Read(origin, destination string) (*Route, error)
	ReadAll() ([]*Route, error)
}
