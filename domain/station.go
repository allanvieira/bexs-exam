package domain

type Station struct {
	Code   string
	Routes []*Route
	Trips  map[string][]Trip
}

type StationService interface {
	ReadStation(route []*Route) ([]*Station, error)
	ClearStation() error
}

type StationRepository interface {
	Insert(station *Station) error
	Update(station *Station) error
	Read(code string) (*Station, error)
	ReadAll() ([]*Station, error)
	Truncate() error
}
