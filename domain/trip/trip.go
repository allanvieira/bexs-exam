package trip

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"fmt"
	"sort"
	"strconv"
)

type service struct {
	stationRepository domain.StationRepository
}

func NewService(stationRepository domain.StationRepository) *service {
	return &service{
		stationRepository: stationRepository,
	}
}

func (s *service) CalcTrips() (stations []*domain.Station, err error) {
	stations = []*domain.Station{}

	allStations, errStation := s.stationRepository.ReadAll()
	if errStation != nil {
		err = errStation
	}

	for _, station := range allStations {
		trips, errTrips := s.followRoutes(*station, domain.Trip{
			Origin:     station.Code,
			Cost:       0,
			RouteLabel: "",
			Routes:     []domain.Route{},
		})
		if errTrips != nil {
			err = errTrips
			return
		}

		sort.Slice(trips, func(i, j int) bool {
			if trips[i].Cost == trips[j].Cost {
				return len(trips[i].Routes) < len(trips[j].Routes)
			}
			return trips[i].Cost < trips[j].Cost
		})

		station.Trips = s.groupTripsByDestination(trips)

		s.stationRepository.Update(station)
	}

	return allStations, nil
}

func (s *service) GetTrip(query domain.TripQuery) (trip *domain.Trip, err error) {

	station, errStation := s.stationRepository.Read(query.Origin)
	if errStation != nil {
		err = errStation
		return
	}

	if station == nil {
		return
	}

	trips, hasDestination := station.Trips[query.Destination]
	if hasDestination {
		trip = &trips[0]
	}

	return
}

func (s *service) GetAllTrip(query domain.TripQuery) (trips []domain.Trip, err error) {

	station, errStation := s.stationRepository.Read(query.Origin)
	if errStation != nil {
		err = errStation
		return
	}

	tripsDestination, hasDestination := station.Trips[query.Destination]
	if hasDestination {
		trips = tripsDestination
	}

	return
}

func (s *service) IsQueryValid(query domain.TripQuery) error {
	if query.Origin == "" {
		return domain.ErrTripQueryEmptyOrigin
	}
	if query.Destination == "" {
		return domain.ErrTripQueryEmptyDestination
	}
	return nil
}

func (s *service) followRoutes(initialStation domain.Station, actualTrip domain.Trip) (trips []*domain.Trip, err error) {
	trips = []*domain.Trip{}

	for _, route := range initialStation.Routes {
		actualTrip.Destination = route.Destination
		routeTrip := actualTrip
		if !s.stationVisited(routeTrip.Routes, route.Destination) {

			actualStation, errStation := s.stationRepository.Read(route.Destination)
			if errStation != nil {
				err = errStation
				return
			}

			routeTrip.Cost = routeTrip.Cost + route.Cost
			routeTrip.Routes = append(routeTrip.Routes, *route)
			routeTrip.RouteLabel = s.tripLabel(routeTrip.Routes, routeTrip.Cost)
			trips = append(trips, &routeTrip)

			nextTrips, errTrips := s.followRoutes(*actualStation, routeTrip)
			if errTrips != nil {
				err = errTrips
				return
			}

			for _, t := range nextTrips {
				trips = append(trips, &domain.Trip{
					Origin:      routeTrip.Origin,
					Destination: t.Destination,
					Cost:        t.Cost,
					RouteLabel:  s.tripLabel(t.Routes, t.Cost),
					Routes:      append(t.Routes),
				})
			}

		}

	}

	return
}

func (s *service) stationVisited(routes []domain.Route, station string) bool {
	for _, r := range routes {
		if r.Origin == station || r.Destination == station {
			return true
		}
	}
	return false
}

func (s *service) tripLabel(routes []domain.Route, cost float64) string {
	if len(routes) == 0 {
		return ""
	}

	label := routes[0].Origin
	for _, r := range routes {
		label += fmt.Sprintf(" - %s", r.Destination)
	}

	return fmt.Sprintf("%s > $%s", label, strconv.Itoa(int(cost)))
}

func (s *service) groupTripsByDestination(trips []*domain.Trip) map[string][]domain.Trip {
	destinations := map[string][]domain.Trip{}

	for _, t := range trips {
		_, hasDestination := destinations[t.Destination]
		if hasDestination {
			destinations[t.Destination] = append(destinations[t.Destination], *t)
		} else {
			destinations[t.Destination] = []domain.Trip{*t}
		}
	}

	return destinations
}
