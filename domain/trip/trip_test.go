package trip_test

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/domain/trip"
	"bitbucket.org/allanvieira/bexs-exam/internal/repository/memory"
	"strconv"
	"testing"
)

var (
	tripService       domain.TripService
	stationRepository = memory.NewStationMemory()
	stationsInitial   = []*domain.Station{{
		Code: "AAA",
		Routes: []*domain.Route{
			{
				Origin:      "AAA",
				Destination: "BBB",
				Cost:        10,
			},
			{
				Origin:      "AAA",
				Destination: "CCC",
				Cost:        10,
			},
			{
				Origin:      "AAA",
				Destination: "DDD",
				Cost:        100,
			}},
		Trips: nil,
	},
		{
			Code:   "BBB",
			Routes: []*domain.Route{},
			Trips:  nil,
		},
		{
			Code: "CCC",
			Routes: []*domain.Route{{
				Origin:      "CCC",
				Destination: "DDD",
				Cost:        32,
			}},
			Trips: nil,
		},
		{
			Code:   "DDD",
			Routes: []*domain.Route{},
			Trips:  nil,
		}}
)

func init() {
	for _, s := range stationsInitial {
		_ = stationRepository.Insert(s)
	}
}

func TestNewService(t *testing.T) {
	t.Run("should return a not nil trip service", func(t *testing.T) {
		tripService = trip.NewService(stationRepository)
		if tripService == nil {
			t.Error("expected not nil service, got nil")
		}
	})
}

func TestService_CalcTrips(t *testing.T) {
	t.Run("should calculate trips using stations routes from repository", func(t *testing.T) {
		stations, err := tripService.CalcTrips()
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}

		if len(stations) != 4 {
			t.Errorf("expected 4 stations, got : %s", strconv.Itoa(len(stations)))
		}

		stationsMap := map[string]*domain.Station{}
		for _, s := range stations {
			stationsMap[s.Code] = s
		}

		bestTripAToD := stationsMap["AAA"].Trips["DDD"][0]
		if bestTripAToD.Cost != 42 {
			t.Errorf("expected trip cost 42 , got : %s", strconv.Itoa(int(bestTripAToD.Cost)))
		}
		if len(bestTripAToD.Routes) != 2 {
			t.Errorf("expected 2 route on trip, got : %s", strconv.Itoa(len(bestTripAToD.Routes)))
		}
		if bestTripAToD.RouteLabel != "AAA - CCC - DDD > $42" {
			t.Errorf("expected trip label 'AAA - CCC - DDD > $42' , got : %s", bestTripAToD.RouteLabel)
		}
		if len(stationsMap["AAA"].Trips) != 3 {
			t.Errorf("expected 3 trip on station a, got : %s", strconv.Itoa(len(stationsMap["AAA"].Trips)))
		}

	})
}

func TestService_GetTrip(t *testing.T) {
	t.Run("should get the best trip A to D", func(t *testing.T) {
		trip, err := tripService.GetTrip(domain.TripQuery{
			Origin:      "AAA",
			Destination: "DDD",
		})
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}

		if trip.RouteLabel != "AAA - CCC - DDD > $42" {
			t.Errorf("expected trip label 'AAA - CCC - DDD > $42' , got : %s", trip.RouteLabel)
		}
		if trip.Cost != 42 {
			t.Errorf("expected trip cost 42 , got : %s", strconv.Itoa(int(trip.Cost)))
		}
		if len(trip.Routes) != 2 {
			t.Errorf("expected 2 route on trip, got : %s", strconv.Itoa(len(trip.Routes)))
		}

	})

	t.Run("should get nil trip", func(t *testing.T) {
		trip, err := tripService.GetTrip(domain.TripQuery{
			Origin:      "ZZZ",
			Destination: "UUU",
		})
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}

		if trip != nil {
			t.Errorf("expected nil trip, got : %v", trip)
		}

	})
}

func TestService_GetAllTrip(t *testing.T) {
	t.Run("should get all trips from station A to D", func(t *testing.T) {
		trips, err := tripService.GetAllTrip(domain.TripQuery{
			Origin:      "AAA",
			Destination: "DDD",
		})
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}

		if len(trips) != 2 {
			t.Errorf("expected 2 trips from a to d , got : %s", strconv.Itoa(len(trips)))
		}
		if trips[0].Cost != 42 {
			t.Errorf("expected best trip cost 42 , got : %s", strconv.Itoa(int(trips[0].Cost)))
		}
		if len(trips[0].Routes) != 2 {
			t.Errorf("expected 2 routes , got : %s", strconv.Itoa(len(trips[0].Routes)))
		}
		if trips[1].Cost != 100 {
			t.Errorf("expected second trip cost 100 , got : %s", strconv.Itoa(int(trips[1].Cost)))
		}
		if len(trips[1].Routes) != 1 {
			t.Errorf("expected 1 route , got : %s", strconv.Itoa(len(trips[1].Routes)))
		}

	})
}

func TestService_IsQueryValid(t *testing.T) {
	t.Run("should got empty valid query", func(t *testing.T) {
		err := tripService.IsQueryValid(domain.TripQuery{
			Origin:      "AAA",
			Destination: "BBB",
		})
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}
	})

	t.Run("should got empty origin error", func(t *testing.T) {
		err := tripService.IsQueryValid(domain.TripQuery{
			Origin:      "",
			Destination: "AAA",
		})
		if err == nil {
			t.Error("expected not nil error, got nil")
		}
		if err != domain.ErrTripQueryEmptyOrigin {
			t.Errorf("expected error empty origin, got : %s", err.Error())
		}
	})

	t.Run("should got empty destination error", func(t *testing.T) {
		err := tripService.IsQueryValid(domain.TripQuery{
			Origin:      "AAA",
			Destination: "",
		})
		if err == nil {
			t.Error("expected not nil error, got nil")
		}
		if err != domain.ErrTripQueryEmptyDestination {
			t.Errorf("expected error empty destination, got : %s", err.Error())
		}
	})

}
