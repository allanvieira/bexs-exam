package trip

import "bitbucket.org/allanvieira/bexs-exam/domain"

type ServiceMock struct {
	CalcTripsInvokedCount    int
	GetTripInvokedCount      int
	GetAllTripInvokedCount   int
	IsQueryValidInvokedCount int

	CalcTripsFn    func() (stations []*domain.Station, err error)
	GetTripFn      func(query domain.TripQuery) (trip *domain.Trip, err error)
	GetAllTripFn   func(query domain.TripQuery) (trips []domain.Trip, err error)
	IsQueryValidFn func(query domain.TripQuery) error
}

func (s *ServiceMock) CalcTrips() (stations []*domain.Station, err error) {
	s.CalcTripsInvokedCount++
	return s.CalcTripsFn()
}

func (s *ServiceMock) GetTrip(query domain.TripQuery) (trip *domain.Trip, err error) {
	s.GetAllTripInvokedCount++
	return s.GetTripFn(query)
}

func (s *ServiceMock) GetAllTrip(query domain.TripQuery) (trips []domain.Trip, err error) {
	s.GetAllTripInvokedCount++
	return s.GetAllTripFn(query)
}

func (s *ServiceMock) IsQueryValid(query domain.TripQuery) error {
	s.IsQueryValidInvokedCount++
	return s.IsQueryValidFn(query)
}
