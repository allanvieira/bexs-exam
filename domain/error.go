package domain

import "errors"

var ErrTripQueryEmptyOrigin = errors.New("inform query param 'origin'")
var ErrTripQueryEmptyDestination = errors.New("inform query param 'destination'")

var ErrRouteEmptyOrigin = errors.New("inform valid attribute 'origin'")
var ErrRouteEmptyDestination = errors.New("inform valid attribute 'destination'")
var ErrRouteInvalidCost = errors.New("invalid attribute 'cost', inform value greater then 0")