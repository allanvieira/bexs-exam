package domain

type Trip struct {
	Origin      string
	Destination string
	Cost        float64
	RouteLabel  string
	Routes      []Route
}

type TripQuery struct {
	Origin      string `json:"origin"`
	Destination string `json:"destination"`
}

type TripService interface {
	CalcTrips() ([]*Station, error)
	GetTrip(query TripQuery) (*Trip, error)
	GetAllTrip(query TripQuery) ([]Trip, error)
	IsQueryValid(query TripQuery) error
}
