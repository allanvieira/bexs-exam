package station

import "bitbucket.org/allanvieira/bexs-exam/domain"

type ServiceMock struct {
	ReadStationInvokedCount  int
	ClearStationInvokedCount int

	ReadStationFn  func(route []*domain.Route) (stations []*domain.Station, err error)
	ClearStationFn func() error
}

func (s *ServiceMock) ReadStation(route []*domain.Route) (stations []*domain.Station, err error) {
	s.ReadStationInvokedCount++
	return s.ReadStationFn(route)
}

func (s *ServiceMock) ClearStation() error {
	s.ClearStationInvokedCount++
	return s.ClearStationFn()
}