package station_test

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/domain/station"
	"bitbucket.org/allanvieira/bexs-exam/internal/repository/memory"
	"strconv"
	"testing"
)

var (
	stationService    domain.StationService
	stationRepository = memory.NewStationMemory()
	routes            = []*domain.Route{
		{
			Origin:      "AAA",
			Destination: "BBB",
			Cost:        10,
		},
		{
			Origin:      "AAA",
			Destination: "CCC",
			Cost:        10,
		},
		{
			Origin:      "CCC",
			Destination: "DDD",
			Cost:        32,
		}}
)

func TestNewService(t *testing.T) {
	t.Run("should return a not nil station services", func(t *testing.T) {
		stationService = station.NewService(stationRepository)
		if stationService == nil {
			t.Error("expected not nil service, got nil")
		}
	})
}

func TestService_ReadStation(t *testing.T) {
	t.Run("should find stations using routes", func(t *testing.T) {
		stations, err := stationService.ReadStation(routes)
		if err != nil {
			t.Errorf("expected not nil error, got : %s", err.Error())
		}

		if len(stations) != 4 {
			t.Errorf("expected 4 stations, got : %s", strconv.Itoa(len(stations)))
		}

		stationsMap := map[string]*domain.Station{}
		for _, s := range stations {
			stationsMap[s.Code] = s
		}

		if len(stationsMap["AAA"].Routes) != 2 {
			t.Errorf("expected 2 route on station AAA, got : %s", strconv.Itoa(len(stationsMap["AAA"].Routes)))
		}
		if len(stationsMap["BBB"].Routes) != 0 {
			t.Errorf("expected 0 route on station BBB, got : %s", strconv.Itoa(len(stationsMap["BBB"].Routes)))
		}
		if len(stationsMap["CCC"].Routes) != 1 {
			t.Errorf("expected 1 route on station CCC, got : %s", strconv.Itoa(len(stationsMap["CCC"].Routes)))
		}
		if len(stationsMap["DDD"].Routes) != 0 {
			t.Errorf("expected 0 route on station DDD, got : %s", strconv.Itoa(len(stationsMap["DDD"].Routes)))
		}

		if stationsMap["CCC"].Routes[0].Cost != 32.0 {
			t.Errorf("expected route cost 32 from B to C, got : %s", strconv.Itoa(int(stationsMap["BBB"].Routes[0].Cost)))
		}

	})
}

func TestService_ClearStation(t *testing.T) {
	t.Run("should truncate the repository", func(t *testing.T) {
		stations, err := stationRepository.ReadAll()
		if err != nil {
			t.Errorf("expected not nil error, got : %s", err.Error())
		}
		if len(stations) != 4 {
			t.Errorf("expected 4 stations on reposiroty, got : %s", strconv.Itoa(len(stations)))
		}

		stationService.ClearStation()

		stations, err = stationRepository.ReadAll()
		if err != nil {
			t.Errorf("expected not nil error, got : %s", err.Error())
		}
		if len(stations) != 0 {
			t.Errorf("expected 0 stations on reposiroty, got : %s", strconv.Itoa(len(stations)))
		}

	})
}
