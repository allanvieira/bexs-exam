package station

import "bitbucket.org/allanvieira/bexs-exam/domain"

type service struct {
	stationRepository domain.StationRepository
}

func NewService(stationRepository domain.StationRepository) *service {
	return &service{
		stationRepository: stationRepository,
	}
}

func (s *service) ReadStation(route []*domain.Route) (stations []*domain.Station, err error) {
	stationCode := map[string]*domain.Station{}
	stations = []*domain.Station{}

	for _, r := range route {
		_, ok := stationCode[r.Origin]
		if !ok {
			stationCode[r.Origin] = &domain.Station{
				Code:   r.Origin,
				Routes: []*domain.Route{r},
				Trips:  nil,
			}
		} else {
			stationCode[r.Origin].Routes = append(stationCode[r.Origin].Routes, r)
		}

		_, okDest := stationCode[r.Destination]
		if !okDest {
			stationCode[r.Destination] = &domain.Station{
				Code:   r.Destination,
				Routes: []*domain.Route{},
				Trips:  nil,
			}
		}
	}

	for _, station := range stationCode {
		stations = append(stations, station)
		errInsert := s.stationRepository.Insert(station)
		if errInsert !=nil {
			err = errInsert
			return
		}
	}

	return
}

func (s *service) ClearStation() error {
	return s.stationRepository.Truncate()
}