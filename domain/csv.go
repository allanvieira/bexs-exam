package domain

type CSVFile interface {
	ReadRows() (rows [][]string, err error)
	OverwriteRows(rows [][]string) (err error)
}