package route_test

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/domain/route"
	"bitbucket.org/allanvieira/bexs-exam/domain/station"
	"bitbucket.org/allanvieira/bexs-exam/domain/trip"
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/csv"
	"bitbucket.org/allanvieira/bexs-exam/internal/repository/memory"
	"errors"
	"strconv"
	"testing"
)

var (
	csvBody = [][]string{
		{"GRU", "ORL", "56"},
		{"ORL", "CDG", "5"},
		{"GRU", "BRC", "10"},
		{"BRC", "SCL", "5"},
		{"GRU", "CDG", "75"},
		{"GRU", "SCL", "20"},
		{"SCL", "ORL", "20"},
	}
	routeService domain.RouteService
	csvMock      = csv.FileMock{
		ReadRowsFn: func() (rows [][]string, err error) {
			return csvBody, err
		},
		OverwriteRowsFn: func(rows [][]string) (err error) {
			csvBody = rows
			return err
		},
	}
	routeRepositoryMock = memory.NewRouteMemory()
	stationServiceMock  = &station.ServiceMock{
		ReadStationFn: func(route []*domain.Route) (stations []*domain.Station, err error) {
			return []*domain.Station{}, nil
		},
		ClearStationFn: func() error {
			return nil
		},
	}
	tripServiceMock = &trip.ServiceMock{
		CalcTripsFn: func() (stations []*domain.Station, err error) {
			return []*domain.Station{}, nil
		},
		GetTripFn: func(query domain.TripQuery) (trip *domain.Trip, err error) {
			return &domain.Trip{}, nil
		},
		GetAllTripFn: func(query domain.TripQuery) (trips []domain.Trip, err error) {
			return []domain.Trip{}, nil
		},
		IsQueryValidFn: func(query domain.TripQuery) error {
			return nil
		},
	}
)

func TestNewService(t *testing.T) {
	t.Run("should create not nil service", func(t *testing.T) {
		routeService = route.NewService(&csvMock, routeRepositoryMock, stationServiceMock, tripServiceMock)
		if routeService == nil {
			t.Error("expected not new service, but got nil")
		}
	})
}

func TestService_ImportCSV(t *testing.T) {
	t.Run("should import data from csv to repository", func(t *testing.T) {
		routes, err := routeService.ImportCSV()
		if err != nil {
			t.Errorf("expected not error, but got : %s", err.Error())
		}

		if len(routes) != 7 {
			t.Errorf("expected 7 routes, but got %s", strconv.Itoa(len(routes)))
		}

		if csvMock.ReadRowsInvokedCount != 1 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(csvMock.ReadRowsInvokedCount))
		}

		if csvMock.OverwriteRowsInvokedCount != 1 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(csvMock.OverwriteRowsInvokedCount))
		}

		routesRepository, errRepository := routeRepositoryMock.ReadAll()
		if errRepository != nil {
			t.Errorf("expected not error, but got : %s", err.Error())
		}

		if len(routesRepository) != 7 {
			t.Errorf("expected 7 routes, but got %s", strconv.Itoa(len(routesRepository)))
		}

	})

	t.Run("should get error on read rows", func(t *testing.T) {
		routeServiceErr := route.NewService(&csv.FileMock{
			ReadRowsFn: func() (rows [][]string, err error) {
				return rows, errors.New("error on open file")
			},
			OverwriteRowsFn: func(rows [][]string) (err error) {
				return nil
			},
		}, routeRepositoryMock, stationServiceMock, tripServiceMock)

		if routeService == nil {
			t.Error("expected not new service, but got nil")
		}

		routes, err := routeServiceErr.ImportCSV()
		if err == nil {
			t.Error("expected not nil err, but got nil")
		}
		if err == errors.New("error on open file") {
			t.Errorf("expected open file error, but got : %s", err.Error())
		}
		if len(routes) > 0 {
			t.Errorf("expected o routes, but got : %s", strconv.Itoa(len(routes)))
		}

	})

	t.Run("should get error on parse float", func(t *testing.T) {
		routeServiceErr := route.NewService(&csv.FileMock{
			ReadRowsFn: func() (rows [][]string, err error) {
				return [][]string{{"CMD", "PTB", "ABC"}}, nil
			},
			OverwriteRowsFn: func(rows [][]string) (err error) {
				return nil
			},
		}, routeRepositoryMock, stationServiceMock, tripServiceMock)

		routes, err := routeServiceErr.ImportCSV()
		if err == nil {
			t.Error("expected not nil err, but got nil")
		}
		if err.Error() != "strconv.ParseFloat: parsing \"ABC\": invalid syntax" {
			t.Errorf("expected parse float error, but got : %s", err.Error())
		}
		if len(routes) > 0 {
			t.Errorf("expected o routes, but got : %s", strconv.Itoa(len(routes)))
		}
	})

}

func TestService_Create(t *testing.T) {
	t.Run("should create new route", func(t *testing.T) {
		inputRoute := &domain.Route{
			Origin:      "ABC",
			Destination: "DEG",
			Cost:        10,
		}
		route, err := routeService.Create(inputRoute)

		if err != nil {
			t.Errorf("expected not error, but got : %s", err.Error())
		}
		if route == nil {
			t.Errorf("expected not nil route, but got nil")
		}

		routeRepository, errRead := routeRepositoryMock.Read("ABC", "DEG")
		if errRead != nil {
			t.Errorf("expected not error but got : %s", err.Error())
		}

		if routeRepository != inputRoute {
			t.Errorf("expected same route, but got distinct")
		}

		if stationServiceMock.ClearStationInvokedCount != 2 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(stationServiceMock.ClearStationInvokedCount))
		}

		if stationServiceMock.ReadStationInvokedCount != 2 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(stationServiceMock.ReadStationInvokedCount))
		}

		if tripServiceMock.CalcTripsInvokedCount != 2 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(tripServiceMock.CalcTripsInvokedCount))
		}

		if csvMock.OverwriteRowsInvokedCount != 2 {
			t.Errorf("expected 1 invoked count, but got %s", strconv.Itoa(csvMock.OverwriteRowsInvokedCount))
		}

	})
}

func TestService_IsValid(t *testing.T) {
	t.Run("should return empty origin", func(t *testing.T) {
		err := routeService.IsValid(&domain.Route{
			Origin:      "",
			Destination: "ABC",
			Cost:        10,
		})

		if err == nil {
			t.Errorf("expected not nil error, but got nil")
		}
		if err != domain.ErrRouteEmptyOrigin {
			t.Errorf("expected empty origin error, but got : %s", err.Error())
		}
	})

	t.Run("should return empty destination", func(t *testing.T) {
		err := routeService.IsValid(&domain.Route{
			Origin:      "ABC",
			Destination: "",
			Cost:        10,
		})

		if err == nil {
			t.Errorf("expected not nil error, but got nil")
		}
		if err != domain.ErrRouteEmptyDestination {
			t.Errorf("expected empty destination error, but got : %s", err.Error())
		}
	})

	t.Run("should return invalid cost", func(t *testing.T) {
		err := routeService.IsValid(&domain.Route{
			Origin:      "ABC",
			Destination: "CDE",
			Cost:        0,
		})

		if err == nil {
			t.Errorf("expected not nil error, but got nil")
		}
		if err != domain.ErrRouteInvalidCost {
			t.Errorf("expected invalid cost error, but got : %s", err.Error())
		}
	})
}
