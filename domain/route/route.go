package route

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"strconv"
)

type service struct {
	csv             domain.CSVFile
	stationService  domain.StationService
	tripService     domain.TripService
	routeRepository domain.RouteRepository
}

func NewService(csv domain.CSVFile, routeRepository domain.RouteRepository, stationService domain.StationService, tripService domain.TripService) *service {
	return &service{
		csv:             csv,
		stationService:  stationService,
		tripService:     tripService,
		routeRepository: routeRepository,
	}
}

func (s *service) ImportCSV() (routes []*domain.Route, err error) {
	routes = []*domain.Route{}

	rows, errRead := s.csv.ReadRows()
	if errRead != nil {
		err = errRead
		return
	}

	for _, r := range rows {

		cost, errCost := strconv.ParseFloat(r[2], 64)
		if errCost != nil {
			err = errCost
			return
		}

		route := &domain.Route{
			Origin:      r[0],
			Destination: r[1],
			Cost:        cost,
		}
		errInsert := s.routeRepository.Insert(route)
		if errInsert != nil {
			err = errInsert
			return
		}

		routes = append(routes, route)
	}

	errUpdateTrips := s.updateTrips()
	if errUpdateTrips != nil {
		err = errUpdateTrips
		return
	}

	return
}

func (s *service) Create(route *domain.Route) (*domain.Route, error) {
	errInsert := s.routeRepository.Insert(route)
	if errInsert != nil {
		return nil, errInsert
	}

	errUpdateTrips := s.updateTrips()
	if errUpdateTrips != nil {
		return nil, errUpdateTrips
	}

	return route, nil
}

func (s *service) IsValid(route *domain.Route) error {
	if route.Origin == "" {
		return domain.ErrRouteEmptyOrigin
	}
	if route.Destination == "" {
		return domain.ErrRouteEmptyDestination
	}
	if route.Cost == 0 {
		return domain.ErrRouteInvalidCost
	}
	return nil
}

func (s *service) updateTrips() error {

	// Clean station repository
	errClean := s.stationService.ClearStation()
	if errClean != nil {
		return errClean
	}

	// Read all routes from repository
	routes, errRoutes := s.routeRepository.ReadAll()
	if errRoutes != nil {
		return errRoutes
	}

	// Identify news station
	_, errStation := s.stationService.ReadStation(routes)
	if errStation != nil {
		return errStation
	}

	// Generate trips and order by cost
	_, errTrips := s.tripService.CalcTrips()
	if errTrips != nil {
		return errTrips
	}

	//Update csv file
	s.csv.OverwriteRows(s.routeToCSV(routes))

	return nil
}

func (s *service) routeToCSV(routes []*domain.Route) [][]string {
	rows := [][]string{}
	for _, r := range routes {
		rows = append(rows, []string{r.Origin, r.Destination, strconv.Itoa(int(r.Cost))})
	}
	return rows
}
