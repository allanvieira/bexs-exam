package route

import "bitbucket.org/allanvieira/bexs-exam/domain"

type ServiceMock struct {
	ImportCSVInvokedCount int
	CreateInvokedCount    int
	IsValidInvokedCount   int

	ImportCSVFn func() (routes []*domain.Route, err error)
	CreateFn    func(route *domain.Route) (*domain.Route, error)
	IsValidFn   func(route *domain.Route) error
}

func (s *ServiceMock) ImportCSV() (routes []*domain.Route, err error) {
	s.ImportCSVInvokedCount++
	return s.ImportCSVFn()
}

func (s *ServiceMock) Create(route *domain.Route) (*domain.Route, error) {
	s.CreateInvokedCount++
	return s.CreateFn(route)
}

func (s *ServiceMock) IsValid(route *domain.Route) error {
	s.IsValidInvokedCount++
	return s.IsValidFn(route)
}
