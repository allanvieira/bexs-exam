package csv

type FileMock struct {
	ReadRowsInvokedCount      int
	OverwriteRowsInvokedCount int

	ReadRowsFn                func() (rows [][]string, err error)
	OverwriteRowsFn           func(rows [][]string) (err error)
}

func (f *FileMock) ReadRows() (rows [][]string, err error) {
	f.ReadRowsInvokedCount++
	return f.ReadRowsFn()
}

func (f *FileMock) OverwriteRows(rows [][]string) (err error) {
	f.OverwriteRowsInvokedCount++
	return f.OverwriteRowsFn(rows)
}
