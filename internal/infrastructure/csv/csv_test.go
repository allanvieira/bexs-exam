package csv_test

import (
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/csv"
	log2 "bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"os"
	"strconv"
	"testing"
)

var (
	pathTest = "test_file.csv"
	csvFile  *csv.File
	log      = log2.NewZeroLog("test", "", "info")
)

func init() {
	file, _ := os.Create(pathTest)
	_, _ = file.WriteString("1,2,3\n4,5,6")
	_ = file.Close()
}

func TestNewService(t *testing.T) {
	t.Run("should return a not nil csv file", func(t *testing.T) {
		csvFileNew, err := csv.New(pathTest, log)
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}
		if csvFileNew == nil {
			t.Error("expected not nil service, got nil")
		}
		csvFile = csvFileNew
	})
}

func TestFile_ReadRows(t *testing.T) {
	t.Run("should read rows from csv file", func(t *testing.T) {
		rows, err := csvFile.ReadRows()
		if err != nil {
			t.Errorf("expected nil error, got : %s", err.Error())
		}
		if len(rows) != 2 {
			t.Errorf("expected 2 rows, got : %s", strconv.Itoa(len(rows)))
		}
		if rows[0][0] != "1" {
			t.Errorf("expected first value from first line '1', got : %s", rows[0][0])
		}
		if rows[0][2] != "3" {
			t.Errorf("expected last value from first line '3', got : %s", rows[0][2])
		}
		if rows[1][0] != "4" {
			t.Errorf("expected first value from second line '4', got : %s", rows[1][0])
		}
		if rows[1][2] != "6" {
			t.Errorf("expected last value from second line '6', got : %s", rows[1][2])
		}
	})

	t.Run("should get error on open", func(t *testing.T) {
		csvFileErr, _ := csv.New("arquivo_invalido.csv", log)

		_, err := csvFileErr.ReadRows()
		if err == nil {
			t.Error("expected not nil erro, got nil")
		}
		if err.Error() != "open arquivo_invalido.csv: no such file or directory" {
			t.Errorf("expected '' error, got : %s", err.Error())
		}

	})
}

func TestFile_OverwriteRows(t *testing.T) {
	t.Run("should replace csv content", func(t *testing.T) {
		err := csvFile.OverwriteRows([][]string{{"0"}})
		if err != nil {
			t.Errorf("expected not nil error, got : %s", err.Error())
		}

		rows, errRead := csvFile.ReadRows()
		if errRead != nil {
			t.Errorf("expected not nil error, got : %s", errRead.Error())
		}

		if len(rows) != 1 {
			t.Errorf("expected 1 rows, got : %s", strconv.Itoa(len(rows)))
		}
		if rows[0][0] != "0" {
			t.Errorf("expected first value from first line '0', got : %s", rows[0][0])
		}
	})

	_ = os.Remove(pathTest)
}