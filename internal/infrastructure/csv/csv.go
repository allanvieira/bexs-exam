package csv

import (
	"bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"encoding/csv"
	"io"
	"os"
)

type File struct {
	filePath string
	log      log.Logger
}

func New(filePath string, log log.Logger) (*File, error) {
	return &File{
		filePath: filePath,
		log:      log,
	}, nil
}

func (f *File) ReadRows() (rows [][]string, err error) {
	rows = [][]string{}

	file, errOpen := os.Open(f.filePath)
	if errOpen != nil {
		f.log.Error("error on open CSV File: %s", err)
		err = errOpen
		return
	}
	defer file.Close()

	IOReader := csv.NewReader(file)
	for {
		record, errRecord := IOReader.Read()
		if errRecord == io.EOF {
			break
		}
		if errRecord != nil {
			err = errRecord
			f.log.Error("error read CSV line : %q", err)
			return
		}
		rows = append(rows, record)
	}

	return
}

func (f *File) OverwriteRows(rows [][]string) (err error) {

	file, errOpen := os.Create(f.filePath)
	if errOpen != nil {
		f.log.Error("error on open CSV File: %s", err)
		err = errOpen
		return
	}
	defer file.Close()

	IOWriter := csv.NewWriter(file)
	defer IOWriter.Flush()

	for _, row := range rows {
		err = IOWriter.Write(row)
		if err != nil {
			f.log.Error("error on write on CSV File")
			break
		}
	}

	return
}
