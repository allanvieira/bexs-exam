package console

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"bufio"
	"fmt"
	"strings"
)

type Server struct {
	console     *bufio.Reader
	tripService domain.TripService
	log         log.Logger
}

func New(console *bufio.Reader, tripService domain.TripService, log log.Logger) *Server {
	return &Server{
		console:     console,
		tripService: tripService,
		log:         log,
	}
}

func (s *Server) ListenAndServe() {

	for {
		fmt.Printf("please enter the route: ")
		input, _ := s.console.ReadString('\n')
		inputs := strings.Split(strings.Replace(input, "\n", "", -1), "-")

		if len(inputs) == 1 {
			fmt.Printf("insert valid origin and destination, example: GRU-CDG \n")
		} else {
			trip, errTrip := s.tripService.GetTrip(domain.TripQuery{
				Origin:      inputs[0],
				Destination: inputs[1],
			})

			if errTrip != nil {
				s.log.Error("Error on get trip : %s", errTrip)
			}

			if trip == nil {
				fmt.Printf("route not found, try new origin or destination\n")
			} else {
				fmt.Printf("best route: %s\n", trip.RouteLabel)
			}
		}

	}

}
