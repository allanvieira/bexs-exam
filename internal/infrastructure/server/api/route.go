package api

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *handler) postRoute(c *gin.Context) {
	route := &domain.Route{}

	if err := c.BindJSON(route); err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": "invalid json body"})
		return
	}

	errValid := h.routeService.IsValid(route)
	if errValid != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": errValid.Error()})
		return
	}

	routeCreated, errCreate := h.routeService.Create(route)
	if errCreate != nil {
		h.log.Error("error on create route : %s", errCreate)
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "contact support team"})
		return
	}

	c.JSON(http.StatusOK, routeCreated)
}
