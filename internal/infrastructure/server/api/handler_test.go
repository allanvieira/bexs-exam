package api_test

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/domain/route"
	"bitbucket.org/allanvieira/bexs-exam/domain/station"
	"bitbucket.org/allanvieira/bexs-exam/domain/trip"
	"bitbucket.org/allanvieira/bexs-exam/internal/infrastructure/server/api"
	log2 "bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"testing"
)

var (
	log             = log2.NewZeroLog("test", "", "info")
	tripServiceMock = trip.ServiceMock{
		CalcTripsFn: func() (stations []*domain.Station, err error) {
			return stations, err
		},
		GetTripFn: func(query domain.TripQuery) (trip *domain.Trip, err error) {
			return trip, err
		},
		GetAllTripFn: func(query domain.TripQuery) (trips []domain.Trip, err error) {
			return trips, err
		},
		IsQueryValidFn: func(query domain.TripQuery) error {
			return nil
		},
	}
	stationServiceMock = station.ServiceMock{
		ReadStationFn: func(route []*domain.Route) (stations []*domain.Station, err error) {
			return stations, err
		},
		ClearStationFn: func() error {
			return nil
		},
	}
	routeServiceMock = route.ServiceMock{
		ImportCSVFn: func() (routes []*domain.Route, err error) {
			return routes, err
		},
		CreateFn: func(route *domain.Route) (*domain.Route, error) {
			return &domain.Route{}, nil
		},
		IsValidFn: func(route *domain.Route) error {
			return nil
		},
	}
)

func TestNewHandler(t *testing.T) {
	t.Run("should create not nil handler", func(t *testing.T) {
		handler := api.NewHandler(log, &tripServiceMock, &stationServiceMock, &routeServiceMock)
		if handler == nil {
			t.Error("should return not nil handler, got nil")
		}
	})
}
