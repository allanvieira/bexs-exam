package api

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"github.com/gin-gonic/gin"
	"net/http"
)

func (h *handler) getTripBest(c *gin.Context) {
	query := domain.TripQuery{
		Origin:      c.Query("origin"),
		Destination: c.Query("destination"),
	}

	errQuery := h.tripService.IsQueryValid(query)
	if errQuery != nil  {
		c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"status": http.StatusBadRequest, "message": errQuery.Error()})
		return
	}

	trip, errTrip := h.tripService.GetTrip(query)
	if errTrip != nil {
		h.log.Error("error on get trip : %s", errTrip.Error())
		c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": http.StatusInternalServerError, "message": "contact support team"})
		return
	}

	if trip == nil {
		c.JSON(http.StatusNotFound, gin.H{"status": http.StatusNotFound, "message": "Trip not found, try new origin or destination"})
		return
	}

	c.JSON(http.StatusOK, trip)

}
