package api

import (
	"bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"context"
	"fmt"
	"net/http"
	"time"
)

type Server struct {
	server *http.Server
	log    log.Logger
}

func New(port string, handler http.Handler, log log.Logger) *Server {
	return &Server{
		server: &http.Server{
			Addr:              fmt.Sprintf(":%s", port),
			Handler:           handler,
			ReadTimeout:       5 * time.Second,
			ReadHeaderTimeout: 55 * time.Second,
		},
		log: log,
	}
}

func (s *Server) ListenAndServe() {
	go func() {
		s.log.Info("Bexs Exam Running on %s!", s.server.Addr)
		if err := s.server.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			s.log.Error("Error on Listen and serve: %s", err)
		}
	}()
}

func (s *Server) Shutdown() {
	s.log.Info("Shutting Down server")
	ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	defer cancel()
	if err := s.server.Shutdown(ctx); err != nil && err != http.ErrServerClosed {
		s.log.Error("Error on shutdown : %s", err)
		return
	}
	s.log.Info("Server has stopped")
}
