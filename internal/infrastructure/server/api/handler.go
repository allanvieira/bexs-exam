package api

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/pkg/log"
	"github.com/gin-gonic/gin"
	"net/http"
)

type handler struct {
	tripService    domain.TripService
	stationService domain.StationService
	routeService   domain.RouteService
	log            log.Logger
}

func NewHandler(log log.Logger, tripService domain.TripService, stationService domain.StationService, routeService domain.RouteService) http.Handler {
	handler := &handler{
		tripService:    tripService,
		stationService: stationService,
		routeService:   routeService,
		log:            log,
	}

	gin.SetMode(gin.ReleaseMode)

	router := gin.New()
	router.Use(handler.recovery())

	v1 := router.Group("/v1")

	// TRIP Handler
	v1.GET("/trip/best", handler.getTripBest)

	// ROUTE Handler
	v1.POST("/route", handler.postRoute)

	return router
}

func (h *handler) recovery() gin.HandlerFunc {
	return func(c *gin.Context) {
		defer func() {
			if err := recover(); err != nil {
				h.log.Error("Fatal err : %s", err)
				c.AbortWithStatusJSON(http.StatusInternalServerError, gin.H{"status": 500, "error": err})
			}
		}()
		c.Next()
	}
}
