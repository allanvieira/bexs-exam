package memory

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"fmt"
)

type Route struct {
	data map[string]*domain.Route
}

func NewRouteMemory() *Route {
	return &Route{
		data: map[string]*domain.Route{},
	}
}

func (r *Route) Insert(route *domain.Route) error {
	r.data[fmt.Sprintf("%s%s",route.Origin, route.Destination)] = route
	return nil
}

func (r *Route) Update(route *domain.Route) error {
	r.data[fmt.Sprintf("%s%s",route.Origin, route.Destination)] = route
	return nil
}

func (r *Route) Read(origin, destination string) (*domain.Route, error) {
	return r.data[fmt.Sprintf("%s%s",origin, destination)], nil
}

func (r *Route) ReadAll() (routes []*domain.Route, err error) {
	routes = []*domain.Route{}
	for _, route := range r.data {
		routes = append(routes, route)
	}

	return
}
