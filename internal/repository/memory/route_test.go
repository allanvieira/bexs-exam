package memory_test

import (
	"bitbucket.org/allanvieira/bexs-exam/domain"
	"bitbucket.org/allanvieira/bexs-exam/internal/repository/memory"
	"testing"
)

var (
	routeStorage = memory.NewRouteMemory()
)

func TestRouteRepository_NewRepository(t *testing.T) {
	if routeStorage == nil {
		t.Fatalf("expected not nil repository, but got : %v", routeStorage)
	}
}

func TestRouteRepository_Insert(t *testing.T) {
	t.Run("should insert two new route successful", func(t *testing.T){
		err1 := routeStorage.Insert(&domain.Route{
			Origin:      "PTB",
			Destination: "CTB",
			Cost:        100,
		})

		err2 := routeStorage.Insert(&domain.Route{
			Origin:      "CTB",
			Destination: "FLN",
			Cost:        150,
		})

		if err1 != nil {
			t.Fatalf("expected nil error, but got : %v", err1)
		}

		if err2 != nil {
			t.Fatalf("expected nil error, but got : %v", err2)
		}

	})

	t.Run("should return two routes", func(t *testing.T){
		routes, err := routeStorage.ReadAll()

		if err != nil {
			t.Fatalf("expected nil error, but got : %v", err)
		}

		totalRoutes := len(routes)
		if totalRoutes != 2 {
			t.Fatalf("expected 2 route, but got : %v", totalRoutes)
		}
	})

}


func TestRouteRepository_Update(t *testing.T) {

	t.Run("should get cost 100 to rout PTB-CTB ", func(t *testing.T){
		route, err := routeStorage.Read("PTB", "CTB")

		if err != nil {
			t.Fatalf("expected nil error, but got : %v", err)
		}
		if route.Cost != 100 {
			t.Fatalf("expected cost 100, but got : %v", route.Cost)
		}
	})

	t.Run("should update route", func(t *testing.T){
		err := routeStorage.Update(&domain.Route{
			Origin:      "PTB",
			Destination: "CTB",
			Cost:        125,
		})
		if err != nil {
			t.Fatalf("expected nil error, but got : %v", err)
		}

		route, err2 := routeStorage.Read("PTB", "CTB")
		if err2 != nil {
			t.Fatalf("expected nil error, but got : %v", err)
		}
		if route.Cost != 125 {
			t.Fatalf("expected cost 125, but got : %v", route.Cost)
		}
	})
}