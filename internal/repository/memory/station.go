package memory

import "bitbucket.org/allanvieira/bexs-exam/domain"

type Station struct {
	data map[string]*domain.Station
}

func NewStationMemory() *Station {
	return &Station{
		data: map[string]*domain.Station{},
	}
}

func (s *Station) Insert(station *domain.Station) error {
	s.data[station.Code] = station
	return nil
}

func (s *Station) Update(station *domain.Station) error {
	s.data[station.Code] = station
	return nil
}

func (s *Station) Read(code string) (*domain.Station, error) {
	return s.data[code], nil
}

func (s *Station) ReadAll() (stations []*domain.Station, err error) {
	stations = []*domain.Station{}
	for _, station := range s.data {
		stations = append(stations, station)
	}

	return
}

func (s *Station) Truncate() error {
	s.data = map[string]*domain.Station{}
	return nil
}